package extra

class Square
{
    // Find magic square of Even Order
    fun magicSquare(n: Int): Unit
    {
        if (n > 0 && n % 2 == 0)
        {
            // Use to collect result
            val matrix: Array < Array < Int >> = Array(n)
            {
                Array(n)
                {
                    0
                }
            };
            // Some auxiliary variable
            var k: Int = n / 2;
            var row: Int = 0;
            var col: Int = k / 2;
            if ((n / 2) % 2 != 0)
            {
                var value: Int = 1;
                // When n is Even and its divisible by 2 reminder is not zero
                // 6,10,14,18,22,26..(4*n +2)
                // Exmple (6/2 = 3) and its reminder not zero
                // Execute loop (n/2)*2 times.
                while (value <= k * k)
                {
                    // Add element in top left grid
                    matrix[row][col] = value;
                    // Add element in bottom right grid
                    matrix[k + row][k + col] = value + (k * k);
                    // Add element in top right grid
                    matrix[row][k + col] = value + (k * k * 2);
                    // Add element in bottom left grid
                    matrix[k + row][col] = value + (k * k * 3);
                    col += 1;
                    row -= 1;
                    if (value % k == 0)
                    {
                        col -= 1;
                        row += 2;
                    }
                    else
                    {
                        if (col == k)
                        {
                            col = 0;
                        }
                        else if (row < 0)
                        {
                            row += k;
                        }
                    }
                    value += 1;
                }
            }
            else
            {
                row = 0;
                // Doubly Even order magic square of n
                // (4*n)
                while (row < n)
                {
                    col = 0;
                    while (col < n)
                    {
                        matrix[row][col] = (n * row) + col + 1;
                        col += 1;
                    }
                    row += 1;
                }
                row = 0;
                while (row < n / 4)
                {
                    col = 0;
                    while (col < n / 4)
                    {
                        // Top Left corner
                        matrix[row][col] = (n * n + 1) - matrix[row][col];
                        col += 1;
                    }
                    row += 1;
                }
                row = 0;
                while (row < n / 4)
                {
                    col = 3 * (n / 4);
                    while (col < n)
                    {
                        // Top right corner
                        matrix[row][col] = (n * n + 1) - matrix[row][col];
                        col += 1;
                    }
                    row += 1;
                }
                row = 3 * n / 4;
                while (row < n)
                {
                    col = 0;
                    while (col < n / 4)
                    {
                        // Bottom Left corner
                        matrix[row][col] = (n * n + 1) - matrix[row][col];
                        col += 1;
                    }
                    row += 1;
                }
                row = 3 * n / 4;
                while (row < n)
                {
                    col = 3 * n / 4;
                    while (col < n)
                    {
                        // Bottom right corner
                        matrix[row][col] = (n * n + 1) - matrix[row][col];
                        col += 1;
                    }
                    row += 1;
                }
                row = n / 4;
                while (row < 3 * n / 4)
                {
                    col = n / 4;
                    while (col < 3 * n / 4)
                    {
                        // Centre elements
                        matrix[row][col] = (n * n + 1) - matrix[row][col];
                        col += 1;
                    }
                    row += 1;
                }
            }
            // Display given size
            println("\n  Magic square of size (" + n + "X" + n + ")");
            row = 0;
            // Display result
            while (row < n)
            {
                col = 0;
                while (col < n)
                {
                    print("  " + matrix[row][col]);
                    col += 1;
                }
                println();
                row += 1;
            }
            k = (n * (n * n + 1) / 2);
            println("  Sum of each rows and columns is " + k);
        }
    }
}
fun main(args: Array < String > ): Unit
{
    val task: Square = Square();
    // Test
    task.magicSquare(6);
    task.magicSquare(10);
    task.magicSquare(4);
    task.magicSquare(8);
}