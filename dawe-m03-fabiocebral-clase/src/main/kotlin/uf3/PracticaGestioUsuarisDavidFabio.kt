package uf3

import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.Scanner
import kotlin.io.path.*
import java.io.IOException
import java.time.LocalDate

/**
 * Procedimiento principal que se encarga de crear la carpeta "data" y el archivo "userData.txt" si no existe. También añade todos los usuarios que hay en import
 * en userData, y finalmente, muestra el menú.
 * @author David, Fabio
 * */
fun main(){
    comprobarSiUserDataExiste() //Si no existe userData.txt lo crea

    //Añade los usuarios de import al archivo userData.txt mientras haya archivos en import
    while (checkIfAreFilesInADirectory("./import")) {
        addToUserData(leerDatosDeImport(getPathOfImportFiles("./import")),"./data/userData.txt", getPathOfImportFiles("./import"))
    }
    //Mientras el usuario no pulse la opción 0("Salir"), se muestra el menú
    do {
        val opcionMenu=printMenu()
        //Dependiendo de la opción que introduzca el usuario, se ejecuta una función u otra
        when (opcionMenu){
            1->createUser("./data/userData.txt")
            2->updateUser("./data/userData.txt")
            3->unlockUser("./data/userData.txt")
            4->addUsersFromOldServer()
            5-> backup()
            0-> { backup() //Cuando salga del programa creará el archivo de backup si no existe, y si existe, lo actualiza
                println("Saliendo del programa...")
            }
        }
    }while (opcionMenu!=0)
}

/**
 * Función que crea una lista con las rutas de los archivos que hay en el directorio "import" y retorna la ruta del primer archivo de la lista.
 * Siempre retorna el primer valor de la lista porque una vez que se pasan los usuarios de import a userData, se borra el archivo de import.
 * @author David, Fabio
 * @param rutaDirectorioImport String que contiene la ruta del directorio "import"
 * @return Retorna un String con la ruta de uno de los archivos que hay en import
 * */
fun getPathOfImportFiles(rutaDirectorioImport:String):String{
    val pathImport = Path(rutaDirectorioImport)
    val listaDeArchivosDeImport:List<Path> = pathImport.listDirectoryEntries()
    return  listaDeArchivosDeImport[0].toString()
}

/**
 * Procedimiento que crea el directorio "data" y el archivo "userData.txt" si no existen
 * @author David, Fabio
 * */
fun comprobarSiUserDataExiste(){
    val rutaDirectorioData = "./data"
    val rutaArchivoUserData = "./data/userData.txt"
    val archivoUserData = File(rutaArchivoUserData)
    //Creo el directorio data si no existe
    Files.createDirectories(Paths.get(rutaDirectorioData))
    //Creo el archivo userData.txt si no existe
    archivoUserData.createNewFile()

}

/**
 * Función que recibe una ruta y devuelve en forma de booleano si el objetivo al cual se apunta existe o no.
 * @author David ,Fabio
 * @param ruta Dirección de la ruta en forma de String.
 * @return Boolean que en caso de ser true confirma que existe la ruta.
 */
fun checkIfPathExists(ruta: String): Boolean {
    val path = Path(ruta)
    return path.exists()
}

/**
 * Funcion que recibe una ruta de un directorio y devuelve si este está vacío o no.
 * @author Fabio,David.
 * @param rutaDirectorio Dirección de la ruta en forma de String.
 * @return Boolean que en caso de existir ficheros en el directorio devuelve true.
 * @exception NoSuchFileException Si le pasas la ruta de un fichero en vez de un directorio por parámetro salta este error.
 */
fun checkIfAreFilesInADirectory(rutaDirectorio:String):Boolean{
    //Si la ruta existe, entonces comprueba que hay archivos en el directorio
    return if (checkIfPathExists(rutaDirectorio)){
        val path = Path(rutaDirectorio)
        //Crea una lista que contiene las rutas de los archivos que están dentro del directorio pasado por parámetro
        val files: List<Path> = path.listDirectoryEntries()
        //Comprueba si la lista de archivos está vacía. Retorna true si tiene archivos y retorna false si está vacía
        files.isNotEmpty()
    }
    else false
}


/**
 * Función que lee los usuarios brutos que contiene el fichero import y los devuelve a una lista mutable de listas de strings, que ya contiene los usuarios válidos.
 *  @author David, Fabio
 *  @param rutaArchivoImport Ruta del fichero dentro de la carpeta import que contiene los usuarios en bruto.
 *  @return Devuelve la lista de los usuarios válidos del antiguo sistema como lista de listas de strings.
 *  @exception IOException Si le pasas la ruta de un directorio y no la de un archivo salta este error.
 */
fun leerDatosDeImport(rutaArchivoImport:String): MutableList<MutableList<String>> {
    val fileImport = File(rutaArchivoImport)
    //Haz Ctrl+s al añadir un nuevo archivo en import en medio de la ejecución del programa
    val text = fileImport.readText() //String que contiene el contenido del archivo de import
    val usuariosBrutosSeparadosPorPuntoYComa = text.split(";")
    val listaDeTodosLosUsuarios = mutableListOf<MutableList<String>>()
    val listaDeLosUsuariosValidos = mutableListOf<MutableList<String>>()
    /*  Bucle que va iterando entre cada usuario y va añadiendo los datos de cada usuario a la lista mutable "listaDeTodosLosUsuarios". De esta manera, ya tenemos la lista separada por usuarios, y dentro de cada usuario,
        hay otra lista que contiene los datos separados en diferentes índices*/
    for (usuario in usuariosBrutosSeparadosPorPuntoYComa){
        listaDeTodosLosUsuarios.add(usuario.split(",") as MutableList<String>)
    }
    /*Bucle que va iterando los índices de la lista que contiene todos los usuarios, y comprueba que el campo "activo" no sea false y el campo "bloqueado" no sea true. Si el usuario es válido, lo añade a la lista de
    usuarios válidos*/
    for (indiceUsuario in listaDeTodosLosUsuarios.indices){
        if (!(listaDeTodosLosUsuarios[indiceUsuario][4] == "false" && listaDeTodosLosUsuarios[indiceUsuario][5] == "true")) {
            listaDeLosUsuariosValidos.add(listaDeTodosLosUsuarios[indiceUsuario])
        }
    }
    return listaDeLosUsuariosValidos
}

/**
 * Procedimiento que recibe los usuarios del antiguo sistema y los escribe en el archivo userData con el Id correspondiente.
 * @author David, Fabio
 * @param listaDeLosUsuariosValidos lista de los usuarios que sí que hemos de añadir, ya que han pasado por el filtro y no tienen los campos en false;true
 * @param rutaArchivoUserData String que contiene la ruta al archivo userData.txt
 * @param rutaArchivoImport String que contiene la ruta al primer archivo de import
 */
fun addToUserData(listaDeLosUsuariosValidos: MutableList<MutableList<String>>, rutaArchivoUserData:String, rutaArchivoImport:String){
    val pathUserData = Path(rutaArchivoUserData)
    val pathImport = Path(rutaArchivoImport)
    val userData = File(rutaArchivoUserData)
    //Bucle que va iterando entre los índices de cada usuario
    for (indiceUsuario in listaDeLosUsuariosValidos.indices){
        //Añado el primer usuario con ID=1 si no hay nada en userData.txt
        if (userData.length() == 0L){
            //Bucle que recorre el índice que tiene cada dato que hay en cada usuario
            for (indiceDato in listaDeLosUsuariosValidos[indiceUsuario].indices){
                if (indiceDato==0) { //Si el iterador está apuntando al primer dato (id), le asigna el id=1 y lo escribe en el fichero "userData.txt"
                    pathUserData.appendText("1")
                    pathUserData.appendText(";")
                }
                //Si no está apuntando al id, le añade el resto de datos del usuario
                else {
                    pathUserData.appendText(listaDeLosUsuariosValidos[indiceUsuario][indiceDato])
                    //Si el iterador todavía no ha llegado al último dato del usuario, escribe un punto y coma (;)
                    if (indiceDato!=listaDeLosUsuariosValidos[indiceUsuario].size-1) {
                        pathUserData.appendText(";")
                    }
                }
            }

        }
        //Si el archivo "userData.txt" no está vacío, le asigna un id al usuario y escribe los demás datos
        else {
            //Bucle que recorre el índice que tiene cada dato que hay en cada usuario
            for (indiceDato in listaDeLosUsuariosValidos[indiceUsuario].indices) {
                if (indiceDato==0){ //Si el iterador está apuntando al primer dato (id), le asigna el id correspondiente y lo escribe en el fichero "userData.txt"
                    val nuevoId = nuevoId("./data/userData.txt").toString() //Llamo a la función nuevoId() que consulta el id del último usuario que hay en "userData.txt" y le suma uno
                    pathUserData.appendText("\n") //Hago un salto de línea para separar a los usuarios
                    //Escribo el id correspondiente y el punto y coma (;)
                    pathUserData.appendText(nuevoId)
                    pathUserData.appendText(";")
                }
                //Si no está apuntando al id, le añade el resto de datos del usuario
                else {
                    pathUserData.appendText(listaDeLosUsuariosValidos[indiceUsuario][indiceDato])
                    //Si el iterador todavía no ha llegado al último dato del usuario, escribe un punto y coma (;)
                    if (indiceDato!=listaDeLosUsuariosValidos[indiceUsuario].size-1) {
                        pathUserData.appendText(";")
                    }
                }
            }
        }
    }
    pathImport.deleteIfExists()//Borra el archivo de import
}

/**
 * Función que devuelve el id que tendrá el próximo usuario añadido. Se encarga de mirar el id que tiene el último usuario que está en "userData.txt" y le suma 1
 * @author Fabio, David
 * @param rutaArchivoUserData ruta al archivo userData.txt
 * @return Devuelve un Int que se le asignará al próximo usuario.
 */
fun nuevoId(rutaArchivoUserData:String):Int{
    val pathUserData = Path(rutaArchivoUserData)
    val text :String = pathUserData.readText() //String que contiene todos los usuarios de userData.txt
    val usuariosActuales=text.split("\n") //Lista que contiene a los usuarios separados, que están en diferentes líneas cada uno
    val datosUsuariosActuales = mutableListOf<List<String>>()
    /*Bucle que va iterando entre cada usuario y va añadiendo los datos de cada usuario a la lista mutable "datosUsuariosActuales". De esta manera, ya tenemos la lista separada por usuarios, y dentro de cada usuario,
    hay otra lista que contiene los datos separados en diferentes índices*/
    for (usuario in usuariosActuales){
        datosUsuariosActuales.add(usuario.split(";"))
    }

    return (datosUsuariosActuales[datosUsuariosActuales.size-1][0].toInt())+1 //Comprueba el id del último usuario de userData.txt y le suma 1
}

/**
 * Procedimiento que le pide al usuario la id de un usuario y si existe cambiará el valor de bloqueado al contrario es decir si es true a false y viceversa.
 * @author David, Fabio
 * @param rutaArchivoUserData ruta al archivo userData.txt
 */
fun unlockUser(rutaArchivoUserData:String){
    val pathUserData = Path(rutaArchivoUserData)
    val text :String = pathUserData.readText() //String que contiene todos los usuarios de userData.txt
    val usuariosActuales=text.split("\n") //Lista que contiene a los usuarios separados, que están en diferentes líneas cada uno
    val datosUsuariosActuales = mutableListOf<MutableList<String>>()
    /*Bucle que va iterando entre cada usuario y va añadiendo los datos de cada usuario a la lista mutable "datosUsuariosActuales". De esta manera, ya tenemos la lista separada por usuarios, y dentro de cada usuario,
    hay otra lista que contiene los datos separados en diferentes índices*/
    for (usuario in usuariosActuales) {
        datosUsuariosActuales.add(usuario.split(";") as MutableList<String>)
    }
    print("Introduce el id del usuario que quieres modificar: ")
    var idIntroducidoExiste = false
    val idIntroducido = readln().toInt()
    //Bucle que itera sobre los índices de cada usuario
    for (usuario in datosUsuariosActuales.indices) {
        //Si el id existe, actualiza el valor del campo "bloqueado" del usuario con ese id
        if (datosUsuariosActuales[usuario][0] == idIntroducido.toString()) {
            idIntroducidoExiste = true
            //Si estaba bloqueado lo desbloquea
            if (datosUsuariosActuales[usuario][5] == "true"){
                datosUsuariosActuales[usuario][5] = "false"
                println("\nUsuario desbloqueado\n")
            }
            //Si estaba desbloqueado lo bloquea
            else {
                datosUsuariosActuales[usuario][5] = "true"
                println("\nUsuario bloqueado\n")
            }
        }
    }
    //Si el id no existe
    if (!idIntroducidoExiste) {
        println("\nEste id no existe\n")
    }
    //Si el id existe
    else {
        pathUserData.writeText("") //Borra el contenido de userData.txt
        addToUserData(datosUsuariosActuales, "./data/userData.txt", "./i")//Y lo reescribe con los datos actualizados
    }
}


/**
 * Procedimiento que siempre que se utilice creara o si existe sobreescribirá el archivo backup con todos los usuarios actuales en un archivo con la fecha actual.
 * @author Fabio, David
 */
fun backup(){
    val userDataPath=Path("./data/userData.txt")
    val text= userDataPath.readText() //String que contiene todos los usuarios de userData.txt
    val fechaHoy=LocalDate.now().toString() //La fecha actual
    val rutaArchivoBackup=File("./backups/${fechaHoy}userData.txt") //Ruta y nombre que tendrá el archivo con el backup
    rutaArchivoBackup.delete() //Borra el backup de hoy
    rutaArchivoBackup.createNewFile() //Lo vuelve a crear
    rutaArchivoBackup.writeText(text) //Y escribimos los usuarios que hay en userData.txt en el archivo con el backup
    println("\nBackup creado con éxito\n")

}

/**
 * Procedimiento que le pide al usuario la id de un usuario y si existe le pedira el nombre, teléfono y mail y cambiara los antiguos valores por los introducidos.
 * @author Fabio, David
 * @param rutaArchivoUserData String que contiene la ruta del archivo userData.txt
 */
fun updateUser(rutaArchivoUserData:String){
    val pathUserData = Path(rutaArchivoUserData)
    val text :String = pathUserData.readText() //String que contiene todos los usuarios de userData.txt
    val usuariosActuales=text.split("\n") //Lista que contiene a los usuarios separados, que están en diferentes líneas cada uno
    val datosUsuariosActuales = mutableListOf<MutableList<String>>()
    /*Bucle que va iterando entre cada usuario y va añadiendo los datos de cada usuario a la lista mutable "datosUsuariosActuales". De esta manera, ya tenemos la lista separada por usuarios, y dentro de cada usuario,
    hay otra lista que contiene los datos separados en diferentes índices*/
    for (usuario in usuariosActuales) {
        datosUsuariosActuales.add(usuario.split(";") as MutableList<String>)
    }
    print("Introduce el id del usuario que quieres modificar: ")
    var idIntroducidoExiste = false
    val idIntroducido = readln()
    //Bucle que itera sobre los índices de cada usuario
    for (usuario in datosUsuariosActuales.indices) {
        //Si el id existe te pide los nuevos datos del usuario
        if (datosUsuariosActuales[usuario][0] == idIntroducido) {
            idIntroducidoExiste = true
            print("Introduce el nuevo nombre de usuario: ")
            val nombreIntroducido = readln()
            print("Introduce el nuevo numero de teléfono: ")
            val numeroDeTelefonoIntroducido = readln()
            print("Introduce el nuevo correo electronico: ")
            val correoIntroducido = readln()
            //Bucle que itera sobre los índices de los datos de cada usuario
            for (indiceDatoUsuario in datosUsuariosActuales[usuario].indices) {
                //Dependiendo del índice al que apunte el iterador, cambia un dato u otro (nombre, teéfono, email)
                when (indiceDatoUsuario) {
                    1 -> datosUsuariosActuales[usuario][indiceDatoUsuario] = nombreIntroducido
                    2 -> datosUsuariosActuales[usuario][indiceDatoUsuario] = numeroDeTelefonoIntroducido
                    3 -> datosUsuariosActuales[usuario][indiceDatoUsuario] = correoIntroducido
                }

            }
        }
    }
    //Si no existe el id
    if (!idIntroducidoExiste) {
        println("\nEste id no existe\n")
    }
    //Si existe el id sobreescribe los nuevos datos
    else {
        pathUserData.writeText("")
        addToUserData(datosUsuariosActuales, "./data/userData.txt", "./i")
        println("\nUsuario actualizado\n")
    }
}

/**
 * Función que muestra el menú por pantalla y deja al usuario acceder a las diferentes funciones del sistema.
 * @author David, Fabio
 * @return Int que corresponde a la opción seleccionada por el usuario
 */
fun printMenu(): Int {
    val scanner =Scanner(System.`in`)
    var opcionMenu:Int
    //Bucle que muestra el menú y pide una opción de menú hasta que se introduzca una opción válida
    do {
        println("### MENU ###\n1-Crear Usuario\n2-Modificar Usuario\n3-(Des)Bloquear Usuario\n4-Añadir usuarios del sistema antiguo\n5-Crear backup\n0-Salir")
        print("Escribe una opcion: ")
        opcionMenu=scanner.nextInt()
        if (opcionMenu !in 0..5) println("\nError, opcion no válida\n")
    }while (opcionMenu !in 0..5)// Para que la opción este dentro del rango.
    return opcionMenu
}

/**
 * Procedimiento que le pide el nombre, email y teléfono al usuario y añade estos datos como un nuevo usuario al fichero userData.txt con el id correspondiente.
 * @author Fabio, David
 * @param  rutaUserData String que contiene la ruta del archivo userData.txt
 */
fun createUser(rutaUserData:String){
    val archivoUsuarios = Path(rutaUserData)
    val userData = File(rutaUserData)
    print("Escribe el nombre del usuario: ")
    val nombreNuevoUsuario = readln()
    print("Escribe el teléfono del usuario: ")
    val telefonoNuevoUsuario = readln()
    print("Escribe el email del usuario: ")
    val emailNuevoUsuario = readln()
    //Si es el primer usuario que se añade al fichero, le pondrá id 1 y los usuarios añadidos siempre estarán con los campos en true y false
    if (userData.length()==0L){
        archivoUsuarios.appendText("1;$nombreNuevoUsuario;$telefonoNuevoUsuario;$emailNuevoUsuario;true;false")
    }
    //Si el archivo userData.txt ya tiene usuarios, añade el usuario con el id correspondiente
    else {
        archivoUsuarios.appendText("\n" + nuevoId("./data/userData.txt") + ";$nombreNuevoUsuario;$telefonoNuevoUsuario;$emailNuevoUsuario;true;false")
    }
    println("\nUsuario creado\n")
}

/**
 * Procedimiento que añade los usuarios del sistema antiguo cuando el usuario lo pide por menu, lo hace archivo a archivo y después lo elimina hasta que no quedan ficheros en import.
 * @author David, Fabio
 */
fun addUsersFromOldServer(){
    //Si no hay archivos en el directorio "import"
    if (!checkIfAreFilesInADirectory("./import")){
        println("\nNo hay usuarios en el sistema antiguo\n")
    }
    //Si hay archivos en el directorio "import"
    else {
        //Mientras haya archivos en "import", se añaden los usuarios de import a userData.txt
        while (checkIfAreFilesInADirectory("./import")) {
            addToUserData(leerDatosDeImport(getPathOfImportFiles("./import")),"./data/userData.txt", getPathOfImportFiles("./import"))
        }
        println("\nUsuarios actualizados con éxito\n")
    }
}