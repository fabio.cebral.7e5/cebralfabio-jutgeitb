# WORDLE (FABIO CEBRAL)
## Explicación del juego
### Se trata del clásico juego Wordle en el cual el jugador introducirá hasta 6 palabras hasta acertarla. Para que sea posible para el usuario, el juego le ofrecerá una serie de pistas. Por ejemplo, si la letra pertenece a la palabra y esta en la posición correcta mostrará la letra en verde, si la letra esta en la palabra pero no es la posición correcta se mostrara la letra en amarillo y si la letra  no pertenece a la palabra se le mostrará en gris. Si en 6 turnos el usuario no es capaz de acertar la palabra perderá.
##### VARIABLES
    input: La palabra introducida por el usuario
    contraseña: La palabra que debe acertar que esta extraida de la lista creada en la parte superior.
    tablero: Es una lista dentro de otra lista que crea el tablero con las palabras introducidas
    turnoJugador: Controla los turnos del usuario.
    partidasJugadas: Número de partidas jugadas por el usuario
    palabrasAcertadas: Número de palabras acertadas por el usuario
    palabrasFalladas: Número de palabras falladas
    rachaMaxima: Racha máxima del jugador sin fallar una palabra
    inWhatTurnYouGuess: Lista que usamos para guardar en que turno acierta el usuario
    seguirJugando: Variable para mirar si quiere jugar mas o no el usuario
    diccionario: Lista de palabras
##### Ampliaciones
    - Ahora el programa funciona con ficheros, es decir las listas de palabras son importadas de un fichero que esta en el directorio diccionarios y es elegido por el usuario.
    - Implementados los idiomas castellano y catalan.
    - Al acabar la partida ahora le pide al usuario un nombre y guardará sus estadísticas en una nueva linea del archivo registro en el directorio registro.
## Conclusión
### Aunque se que esto no es necesario me gustaría ofrecerte mi feedback acerca de la actividad, en mi experiencia personal ha sido una actividad muy divertida, aunque he visto a muchos de mis compañeros muy extresados con la práctica creo que si te esfuerzas la consigues y ademas siempre resuelves todas las dudas personales que puedes. Espero que las actividades mantengan esta línea.