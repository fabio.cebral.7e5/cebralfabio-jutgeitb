package wordleConFicheros

import java.io.File
import java.util.Scanner

const val abecedario="abcdefghijklmnopqrstuvwxyz"

/**
 * Función main que contiene todas las variables y que deja al usuario elegir el idioma y a continuación jugar tantas veces como el quiera mientras haya palabras.
 * @author Fabio Cebral
 *
 */
fun main(){
    //VARIABLES
val scanner=Scanner(System.`in`)
    //Variables de las partidas jugadas
    var partidasJugadas=0
    //Variables de las partidas en las que el usuario ha acertado la palabra
    var palabrasAcertadas=0
    //Variables de las partidas en las que el usuario no ha acertado la palabra
    var palabrasFalladas=0
    //Variables de la cuenta la racha actual
    var rachaActual=0
    //Variable de la racha mas
    var rachaMaxima=0
    //Variable que cuenta los turnos del jugador
    var turnosJugador:Int
    //Lista en la que cada posición es el turno en el que acierta el usuario y se usa para calcular el porcentaje por turno
    val inWhatTurnYouGuess= mutableListOf(0,0,0,0,0,0)
    //Variable que contiene la palabra a acertar por el usuario
    var contraseña:String
    //Lista que contiene todas las palabras que va añadiendo el usuario y las va pintando
    val tablero= mutableListOf<String>()
    //String el cual introduce el usuario
    var input:String
    //Booleano que muestra la intención del usuario de si el usuario quiere jugar mas o no.
    var seguirJugando=true
    //Las palabras dependiendo del idioma que selecciona el usuario
    val diccionario = getDiccionario() as MutableList<String>
    do {
        //RESETEAMOS EL JUEGO
        contraseña=diccionario.random()
        println(contraseña)
        tablero.clear()
        turnosJugador=6
    do {
        printMenu(turnosJugador)
        input=scanner.next()
        while (input.length!=5){
            println("Longitud incorrecta vuelva a introducir")
            input=scanner.next()
        }
        tablero.add(input)
        //MOSTRAMOS LOS INPUTS DEL USUARIO CON LOS COLORES
        for (i in tablero){
            writeWords(contraseña.lowercase(),i.lowercase(), getIndicesOfAWord(contraseña.lowercase()), getIndicesOfAWord(i.lowercase()))
            println()
        }
        turnosJugador--
        println()
    } while (input.lowercase()!=contraseña.lowercase() && turnosJugador>0)
    partidasJugadas++
        //CUANDO ACIERTA LA PALABRA
    if (input.lowercase()==contraseña.lowercase()){
        println("ACERTASTEE")
        palabrasAcertadas++
        rachaActual++
        if (rachaActual>rachaMaxima)rachaMaxima=rachaActual
        when(turnosJugador){
         5-> inWhatTurnYouGuess[0]++
         4-> inWhatTurnYouGuess[1]++
         3-> inWhatTurnYouGuess[2]++
         2-> inWhatTurnYouGuess[3]++
         1-> inWhatTurnYouGuess[4]++
         0-> inWhatTurnYouGuess[5]++
        }
    } //SI NO LA ACIERTA
    else {
        if (rachaActual>rachaMaxima)rachaMaxima=rachaActual
        rachaActual=0
        println("Perdiste, la palabra era $contraseña")
        palabrasFalladas++
    }
        //MOSTRAMOS LAS ESTADÍSTICAS
getStats(partidasJugadas,palabrasAcertadas,palabrasFalladas,inWhatTurnYouGuess,rachaMaxima)
        diccionario.remove(contraseña)
        //SE LE PREGUNTA SI QUIERE VOLVER A JUGAR
        println("1-Jugar otra partida\n2-Salir del juego")
        var jugarOtra=scanner.nextInt()
        while (jugarOtra>2 || jugarOtra<1){
            println("Vuelve a introducir")
            jugarOtra=scanner.nextInt()
        }
        if (jugarOtra==2) seguirJugando=false
    } while (seguirJugando || diccionario.isEmpty())
    println("Introduce tu nombre de jugador")
    val nombre=scanner.next()
    writeGame(nombre,partidasJugadas,palabrasAcertadas,palabrasFalladas,rachaMaxima)
}
/**
 *Función que muestra el menu cuando se le llama.
 * @author Fabio Cebral
 * @param turnos Los turnos del usuario actuales
 */
fun printMenu(turnos:Int){
    if (turnos==6){
        println("Bienvenido")
    }
    print("Te quedan $turnos turnos\nIntroduce una palabra de 5 letras: ")
}

/**
 * Funcion que devuelve que letras y cuantas de cada tiene la palabra que le introduzcamos por parámetro.
 * @param palabra La palabra la cual queremos saber sus letras
 * @return abecedarioIndices La lista en la que cada posición es una letra y nos dirá de cuales hay cuantas
 */
fun getIndicesOfAWord(palabra:String): MutableList<Int> {
    val abecedarioIndices= mutableListOf(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
for (i in abecedario.indices){
    for (y in palabra.indices){
        if (abecedario[i]==palabra[y]){
            abecedarioIndices[i]++
        }
    }
}
    return abecedarioIndices
}
/**
 * Función que pinta el input del usuario acorde a las reglas acordes al wordle original.
 * @author Fabio Cebral
 * @param contraseña La palabra a adivinar
 * @param input El input del usuario
 * @param abecedarioIndicesContraseña Lista con las letras que tiene la palabra secreta
 * @param abecedarioIndicesInput Lista con las letras que tiene el input
 */
fun writeWords(contraseña: String,input:String,abecedarioIndicesContraseña:MutableList<Int>,abecedarioIndicesInput: MutableList<Int>){
    var posicionEnElAbecedario:Int
for (i in contraseña.indices){
    posicionEnElAbecedario= abecedario.indexOf(input[i])

    if (contraseña[i]==input[i]) {
        print(ANSI_GREEN_BACKGROUND + ANSI_BLACK + input[i] + ANSI_RESET + " ")
        //Resta 1 a la cantidad de veces que está la letra en el input y en la palabra secreta
        abecedarioIndicesContraseña[posicionEnElAbecedario] -=1
        abecedarioIndicesInput[posicionEnElAbecedario] -=1
    } else if (input[i] in contraseña && abecedarioIndicesContraseña[posicionEnElAbecedario] >= abecedarioIndicesInput[posicionEnElAbecedario]) {
        print(ANSI_YELLOW_BACKGROUND +ANSI_BLACK + input[i] + ANSI_RESET + " ")
        //Resta 1 a la cantidad de veces que está la letra en el input
        abecedarioIndicesInput[posicionEnElAbecedario] -= 1
    } else if (input[i] in contraseña) {
        print(ANSI_WHITE_BACKGROUND +ANSI_BLACK + input[i] + ANSI_RESET + " ")
        //Resta 1 a la cantidad de veces que está la letra en el input
        abecedarioIndicesInput[posicionEnElAbecedario] -=1
    }else if (input[i] !in contraseña) {
        print(ANSI_WHITE_BACKGROUND +ANSI_BLACK + input[i] +  ANSI_RESET + " ")
    }
}
}
/**
 * Función que genera y imprime las estadísticas del usuario.
 * @author Fabio Cebral
 * @param palabrasAcertadas Las palabras acertadas
 * @param palabrasFalladas Las palabras falladas
 * @param partidasJugadas Las partidas jugadas
 * @param rachaMaxima La racha máxima
 * @param inWhatTurnYouGuess Lista con el turno el que acierta el archivo
 */
fun getStats(partidasJugadas:Int,palabrasAcertadas:Int,palabrasFalladas:Int,inWhatTurnYouGuess:MutableList<Int>,rachaMaxima:Int){
println("###Estadísticas###\nPartidas Jugadas:$partidasJugadas\nPalabras Acertadas:$palabrasAcertadas\nPalabras falladas:$palabrasFalladas\nRacha Máxima: $rachaMaxima")
    if (palabrasAcertadas>0){
        print("Porcentaje de acierto: ${(palabrasAcertadas*100)/partidasJugadas}%\nPorcentaje de acierto en el primer turno: ${(inWhatTurnYouGuess[0]*100)/palabrasAcertadas}%\n" +
                "Porcentaje de acierto en el segundo turno: ${(inWhatTurnYouGuess[1] * 100) / palabrasAcertadas}%\n" +
                "Porcentaje de acierto en el tercer turno: ${(inWhatTurnYouGuess[2] * 100) / palabrasAcertadas}%\n" +
                "Porcentaje de acierto en el cuarto turno: ${(inWhatTurnYouGuess[3] * 100) / palabrasAcertadas}%\n" +
                "Porcentaje de acierto en el quinto turno: ${(inWhatTurnYouGuess[4] * 100) / palabrasAcertadas}%\n" +
                "Porcentaje de acierto en el sexto turno: ${(inWhatTurnYouGuess[5] * 100) / palabrasAcertadas}%\n")
    }
}

/**
 * Lee el archivo del idioma seleccionado por el usuario 'catalan' o 'castellano' y devuelve una lista con todas las palabras
 * @author Fabio Cebral
 * @return fileDiccionario Lista con todas las palabras
 */
fun getDiccionario(): List<String> {
    val scanner = Scanner(System.`in`)
    println("¿En que idioma quieres jugar catalan o castellano?")
    var idioma = scanner.next()
    while (idioma != "castellano" && idioma != "catalan") {
        println("Opcion incorrecta, recuerda catalan o castellano")
        idioma = scanner.next()
    }
    val fileDiccionario = File("./diccionarios/$idioma.txt")
    return fileDiccionario.readLines()
}

/**
 * Función que escribe en el archivo los datos y el nombre de usuario al acabar la partida
 * @author Fabio Cebral
 * @param nombreUsuario Nombre al que se guaradará la partida
 * @param palabrasAcertadas Las palabras acertadas
 * @param palabrasFalladas Las palabras falladas
 * @param partidasJugadas Las partidas jugadas
 * @param rachaMaxima La racha máxima
 */
fun writeGame(nombreUsuario:String  ,partidasJugadas:Int,palabrasAcertadas:Int,palabrasFalladas:Int,rachaMaxima:Int){
    val file = File("./registro/registro.txt")
    file.appendText("$nombreUsuario,$partidasJugadas,$palabrasAcertadas,$palabrasFalladas,$rachaMaxima${(palabrasAcertadas*100)/partidasJugadas}\n")
}