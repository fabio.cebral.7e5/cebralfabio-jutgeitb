package uf2

fun marcarCasilla(fila:Int, columna:Int, tablero:List<MutableList<String>>, turno:Int ): List<MutableList<String>> {
    for (i in 0 .. 2){
        for (j in 0 .. 2){
            if (i==fila && j==columna && turno%2!=0){
                tablero[i][j]="x"
            }
            else if (i==fila && j==columna && turno%2==0){
                tablero[i][j]="o"
            }
        }
    }
    return tablero

}
fun chequeoHorizontal(tablero: List<MutableList<String>>): Boolean {
    var racha=0
    var rachaO=0
    var ganador=false
    for (i in 0 until tablero.size){
        racha=0
        for (j in 0 until tablero.size){
            if (tablero[i][j]=="x") racha++
            else if (racha==3) !ganador
            if (tablero[i][j]=="o") rachaO++
            else if (rachaO==3) !ganador
        }

    }

    return ganador
}
fun chequeoVertical(tablero: List<MutableList<String>>): Boolean {
    var ganador=false
    for (i in 0 until tablero.size){
        if (tablero[i][0]=="x" && tablero[i][0]=="x" && tablero[i][0]=="x" ){
            !ganador
        }
        else if (tablero[i][1]=="x" && tablero[i][1]=="x" && tablero[i][1]=="x" ){
            !ganador
        }
        else if (tablero[i][2]=="x" && tablero[i][2]=="x" && tablero[i][2]=="x" ){
            !ganador
        }
        else if (tablero[i][0]=="o" && tablero[i][0]=="o" && tablero[i][0]=="o" ){
            !ganador
        }
        else if (tablero[i][1]=="o" && tablero[i][1]=="o" && tablero[i][1]=="o" ){
            !ganador
        }
        else if (tablero[i][2]=="o" && tablero[i][2]=="o" && tablero[i][2]=="o" ){
            !ganador
        }
    }
    return ganador
}
fun chequeoDiagonal(tablero: List<MutableList<String>>):Boolean {
    var ganador=false
    if (tablero[0][0]=="x" && tablero[1][1]=="x" && tablero[2][2]=="x" ) ganador=true
    if (tablero[0][2]=="x" && tablero[1][1]=="x" && tablero[2][0]=="x" ) ganador=true
    if (tablero[0][0]=="o" && tablero[1][1]=="o" && tablero[2][2]=="o" ) ganador=true
    if (tablero[0][2]=="o" && tablero[1][1]=="o" && tablero[2][0]=="o" ) ganador=true

    return ganador
}
fun imprimirTablero(tablero: List<MutableList<String>>){
    for (i in 0 until tablero.size){
        for (j in tablero.indices){
            print(tablero[i][j])
        }
        println()
    }
}


fun main() {
    var tablero = List(3) { mutableListOf(".", ".", ".") }
    println(tablero[1])
    var fila: Int
    var columna: Int
    var turno = 1
    var ganar: Boolean = false
    do {
        println("Introduce la fila . ")
        fila = readln().toInt()
        while (fila < 0 || fila > 2) {
            println("Valor incorrecto . ")
            fila = readln().toInt()
        }
        println("Introduce columna . ")
        columna = readln().toInt()
        while (columna < 0 || columna > 2) {
            println("Valor incorrecto . ")
            columna = readln().toInt()
        }
        marcarCasilla(fila, columna, tablero, turno)
        imprimirTablero(tablero)

        ganar = chequeoHorizontal(tablero)
        if (!ganar) {
            ganar = chequeoVertical(tablero)
        }
        if (!ganar) {
            ganar = chequeoDiagonal(tablero)
        }

        turno++

    } while (!ganar && turno < 9)
    if (turno > 8 && !ganar) {
        println("Empate fieras")
    }
    if (turno % 2 == 0 && ganar == true) {
        println("Has ganado jugador *x* ")
    } else if (turno % 2 != 0 && ganar == true) println("Has ganado jugador *o* ")
}

