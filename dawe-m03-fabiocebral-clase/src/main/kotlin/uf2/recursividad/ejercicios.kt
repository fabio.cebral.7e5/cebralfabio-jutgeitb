package uf2.recursividad

fun main() {
    println(creixents(12356))
}
fun factorial (num1:Int):Int {
    return when{
    num1<0 -> 0
    num1==0 -> 1
    else -> num1 *  factorial(num1-1)
    }
    }
fun dobleFactorial(num1:Int):Int {
  return when {
      num1 < -1 -> 0
      num1==0 -> 1
      else -> num1 * dobleFactorial(num1-2)
  }
}
fun nombreDigits(numero:Int): Int {
if (numero==0) return 0
    else (
        return 1+nombreDigits(numero/10)
    )

}

fun creixents(num: Int):Boolean {
    return creixents_req(num.toString(), 0)

}

fun creixents_req(numString: String, posicion: Int): Boolean {
    println(numString)
    val posicionSiguiente = posicion+1
    val a = numString[posicion]
    val b = numString[posicionSiguiente]
    var comparar: Boolean = a <= b
    println("$a $b $comparar")

    if (posicionSiguiente+1==numString.length){
        return comparar
    }
    return comparar && creixents_req(numString, posicion+1)
}


