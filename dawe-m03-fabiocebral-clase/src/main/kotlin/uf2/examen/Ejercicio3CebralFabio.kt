package uf2.examen

import java.util.Scanner

//definició dels colors que farem servir, agafant els backgrounds ANSI, i establint-los com a constants globals
const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[44m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"

fun main(){
    val colors = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED, ANSI_YELLOW, ANSI_BLUE, ANSI_PURPLE, ANSI_CYAN)
    var color=1
    var intensitat=1
    var intensitatPujada=true
    var saveColor=0
    do {
        print("$ANSI_RESET Introdueix l'ordre ordre: ")
        var instruc= readln()
        when(instruc) {
            "TURN ON" -> {
                if(saveColor==0){
                    color=1
                    intensitat=1
                    intensitatPujada=true
                }else {
                    color=saveColor
                    intensitat=1
                    intensitatPujada=true
                }
            }
            "TURN OFF" -> {
                if(color>0){
                   color=0
                    intensitat=0
                }
            }
            "CHANGE COLOR" -> {
                if (color<colors.size) {
                    color++
                    saveColor=color
                }
                else if(color==colors.size) color=1
            }
            "INTENSITY" -> {
                if (intensitatPujada) intensitat++
                else if (intensitat==5){
                    intensitat==1
                } else if (!intensitatPujada && intensitat>1) intensitat--
                else if (!intensitatPujada && intensitat==1){
                    intensitat++
                }
            }
        }
        println("Color: ${colors[color]}    $ANSI_BLUE - intensitat $intensitat")
    } while(instruc!="END")
}