package uf2.examen

import java.util.Scanner

fun lecturasSobreFcm(dades: Array<String>, edad: Int){
    var dadesToInt = mutableListOf<Int>()
    for (i in dades){
        dadesToInt.add(i.toInt())
    }
    for (i in dadesToInt){
        if (i>edad){
            print("$i" + " ")
        }
    }
}
fun sotaSuau(dades: Array<String>, edad:Int){
    var min=(edad*60)/100
    var max=(edad*69)/100
    var dadesToInt = mutableListOf<Int>()
    for (i in dades){
        dadesToInt.add(i.toInt())
    }
    for (i in dadesToInt){
        if (i<min){
            print(i)
        }
    }

}
fun nivellMaxim(dades: Array<String>, edad: Int){
    var min=(edad*90)/100
    var max=(edad*100)/100
    var dadesToInt = mutableListOf<Int>()
    var atTheLevel= mutableListOf<Int>()
    for (i in dades){
        dadesToInt.add(i.toInt())
    }
    for (i in dadesToInt){
        if (i>=min && i<=max){
atTheLevel.add(i)        }
    }
    val porcentaje=(atTheLevel.size*100)/dades.size
    println("$porcentaje%")
}
fun nivellIntens(dades: Array<String>, edad: Int){
    var min=(edad*80)/100
    var max=(edad*89)/100
    var dadesToInt = mutableListOf<Int>()
    var atTheLevel= mutableListOf<Int>()

    for (i in dades){
        dadesToInt.add(i.toInt())
    }
    for (i in dadesToInt){
        if (i>=min && i<=max){
            atTheLevel.add(i)
        }
    }
    val porcentaje=(atTheLevel.size*100)/dades.size
    println("$porcentaje%")
}
fun nivellModerat(dades: Array<String>, edad: Int){
    var min=(edad*70)/100
    var max=(edad*79)/100
    var dadesToInt = mutableListOf<Int>()
    var atTheLevel= mutableListOf<Int>()
    for (i in dades){
        dadesToInt.add(i.toInt())
    }
    for (i in dadesToInt){
        if (i>=min && i<=max){
            atTheLevel.add(i)
        }
    }
    val porcentaje=(atTheLevel.size*100)/dades.size
    println("$porcentaje%")
}
fun nivellSuau(dades: Array<String>, edad: Int){
    var min=(edad*60)/100
    var max=(edad*69)/100
    var dadesToInt = mutableListOf<Int>()
    var atTheLevel= mutableListOf<Int>()
    for (i in dades){
        dadesToInt.add(i.toInt())
    }
    for (i in dadesToInt){
        if (i>=min && i<=max){
            atTheLevel.add(i)
        }
    }
    val porcentaje=(atTheLevel.size*100)/dades.size
    println("$porcentaje%")
}

fun nivellMoltSuau(dades: Array<String>, edad: Int){
    var min=(edad*50)/100
    var max=(edad*59)/100
    var dadesToInt = mutableListOf<Int>()
    var atTheLevel= mutableListOf<Int>()
    //Pasar la lista a Int Type
    for (i in dades){
        dadesToInt.add(i.toInt())
    }
    for (i in dadesToInt){
        if (i>=min && i<=max){
            atTheLevel.add(i)
        }
    }
    val porcentaje=(atTheLevel.size*100)/dades.size
    println("$porcentaje%")
}
//Devuelve el fcm
fun calcularFCM(edad:Int): Int {
    return 220-edad
}

fun mostrarMaxAndMin(dades: Array<String>){
    var dadesToInt = mutableListOf<Int>()
    for (i in dades){
        dadesToInt.add(i.toInt())
    }
    println("Max: " + dadesToInt.max())
    println("Min: " + dadesToInt.min())
}

fun mostrarMitjaDades(dades: Array<String>): Double {
   var dadesToInt = mutableListOf<Int>()
    for (i in dades){
        dadesToInt.add(i.toInt())
    }
    return dadesToInt.average()
}

fun mostrarQuantitatMesures(dades: Array<String>){
    println(dades.size)
}

fun mostrarDades(dades: Array<String>){
    for ( i in dades){
        print(i + " ")
    }
}
fun menu(){
    println("\n" +
            "1- Mostrar dades llegides de l’activitat\n" +
            "2- Mostrar quantitat mesures de l’activitat\n" +
            "3- Mostrar freqüència cardíaca mitjana de l’activitat\n" +
            "4- Mostrar freqüències cardíaques mínima i màxima de l’activitat\n" +
            "5- Aprofitament de l’activitat (aquesta opció mostrarà un submenú)\n0- Tornar al menú principal\n")
}
//Funcion del submenu maneja lo que quiere hacer el usuario
fun subMenu(dades: Array<String>){
    val scanner=Scanner(System.`in`)
    var input:Int
    var edad=0
    var fcm=0
    do {
        println(
            "1- Introduir edat usuari\n" +
                    "2- Nivell molt suau: mostrar número de lectures i % entre el 50% i el 59% de la FCM.\n" +
                    "3- Nivell suau: mostrar número de lectures i % entre el 60% i el 69% de la FCM.\n" +
                    "4- Nivell moderat: mostrar número de lectures i % entre el 70% i el 79% de la FCM.\n" +
                    "5- Nivell intens: mostrar número de lectures i % entre el 80% i el 89% de la FCM.\n" +
                    "6- Nivell màxim: mostrar número de lectures i % entre el 90% i el 100% de la FCM.\n" +
                    "7- Mostrar lectures per sota del nivell suau\n" +
                    "8- Mostrar lectures per sobre de la FCM\n" +
                    "0- Tornar al menú principal\n"
        )
        input = scanner.nextInt()
        //Control de datos para el input
        while (input < 0 || input > 8) {
            println("Valor incorrecto, vuelve a introducir")
            input = scanner.nextInt()
        }
        when (input) {
            //Para controlar que el usuario no introduce una edad negativa
            1 -> {
                println("Introduce tu edad")
                edad = scanner.nextInt()
                while (edad < 0) {
                    println("Edad inexistente")
                    edad = scanner.nextInt()
                }
                fcm = calcularFCM(edad)
            }
                2 -> nivellMoltSuau(dades, fcm)
                3 -> nivellSuau(dades, fcm)
                4 -> nivellModerat(dades, fcm)
                5 -> nivellIntens(dades, fcm)
                6 -> nivellMaxim(dades, fcm)
                7 -> sotaSuau(dades, fcm)
                8 -> lecturasSobreFcm(dades, fcm)

        }
    }while (input!=0)
}
//Funcion principal que hace funcionar el programa utilizando las diversas funciones
fun freakKit(dades:Array<String>){
    val scanner=Scanner(System.`in`)
    var input:Int
    do {
        menu()
        input=scanner.nextInt()
        while (input<0 || input>5){
            println("Valor incorrecto, vuelve a introducir")
            input=scanner.nextInt()
        }
        when (input){
            1-> mostrarDades(dades)
            2-> mostrarQuantitatMesures(dades)
            3-> mostrarMitjaDades(dades)
            4-> mostrarMaxAndMin(dades)
            5-> subMenu(dades)
        }

    }while (input!=0)
}
fun main(args:Array<String>) {
freakKit(args)
}