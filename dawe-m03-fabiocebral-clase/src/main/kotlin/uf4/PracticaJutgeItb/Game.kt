package uf4.PracticaJutgeItb

import wordleConFicheros.ANSI_GREEN
import wordleConFicheros.ANSI_RED
import wordleConFicheros.ANSI_RESET
import java.util.Scanner

fun main() {
    var turnos= mutableListOf<Int>(0,0,0,0,0)
    val scanner=Scanner(System.`in`)
    var seguirJugando=true
    //Creación de los problemas todos con la clase Problema
val suma2numeros=Problema("Problema 1\nUn programa que resta dos numeros enteros y devuelve el resultado.\n${ANSI_GREEN}Input=5,3 Output=2 || Input=6,3 Output=3\n","Cuanto es 6307 - 4622?","1685",1,false, mutableListOf())
val sumaCostadosDeFigurasGeometricas=Problema("Problema 4\nEn este programa tendras que sumar los costados de estas figuras y devolver el total.\n${ANSI_GREEN}Input= 'Cuadrado,Triangulo' Output= '7' || Input='Circunferencia,Circunferencia' Output = 2\n","Cuantos costados tiene la suma de las siguientes figuras 'Pentagono,Rombo,Trapecio,Eneágono,Deltoide'?","26",1,false,mutableListOf())
val elevaElPrimeroAlSegundoNumero=Problema("Problema 2\nAhora tendrás que calcular la potencia un número elevado por otro.\n${ANSI_GREEN}Input=3,3 Output= 27 || Input=2,5 Output= 32\n","Calcula la potencia de 5 y 12","244140625",1,false,mutableListOf())
val esPar=Problema("Problema 3\nAhora recibiremos un número y si es par la respuesta correcta será un 1 y si es impar será un 2\n${ANSI_GREEN}Input=2 Output=1 || Input=5 Output=2\n","Es el numero 145634 par?","1",1,false, mutableListOf())
    val calculaElResto=Problema("Problema 5\nCalcula el resto de la división que se plantea\n" +
            "${ANSI_GREEN}Input=7/5 Output=2 || Input=8/4 Output=0\n","Calcula el resto de 11/2","1",1,false, mutableListOf())
    //Almaceno todos los objetos instanciados en una misma lista para poder recorrerla.
val problemas= mutableListOf(suma2numeros,elevaElPrimeroAlSegundoNumero,esPar,sumaCostadosDeFigurasGeometricas,calculaElResto)
    var wantToGuess:Int
        for (i in problemas) {
            i.toString()
            wantToGuess = scanner.nextInt()
            while (wantToGuess > 2 || wantToGuess < 1) {
                println("Opcion incorrecta, vuelve a introducir")
                wantToGuess = scanner.nextInt()
            }
            if (wantToGuess == 1) {
                i.showProblem()
                var respuesta: String
                do {

                    respuesta = scanner.next()
                    if (respuesta == "skip") {
                        break
                    }
                    i.respuestasIntroducidas.add(respuesta)
                    if (respuesta != i.respuesta) {
                        println("${ANSI_RED}No es correcto, vuelve a probar")
                        i.turnos++
                        print("${ANSI_RESET}Tus respuestas: ")
                        for (y in i.respuestasIntroducidas) {
                            print(y + " ")
                        }
                        println()
                    }
                } while (respuesta != i.respuesta)
                if (respuesta != i.respuesta) {
                    println("${ANSI_RESET}Bueno pasamos con el siguiente")
                } else if (respuesta==i.respuesta){
                    println("Bien has solucionado el problema")
                    i.resolt = true
                }
            }
            showStats(problemas, getTurns(turnos,problemas))
        }
}
/**
 * Función muestra por pantalla las estadísticas del usuario en el juego.
 * @author Fabio Cebral
 * @param turnos Lista en la que cada posicion es el turno del problema en esa posición
 * @param problemas Lista con todos los problemas
 */
fun showStats(problemas:MutableList<Problema>,turnos: MutableList<Int>){
    var resueltos=0
    var problemasResueltos=""
    var turnosTotales=0
    for (i in problemas.indices){
        if (problemas[i].resolt){
            resueltos++
            problemasResueltos+=i
            turnosTotales+=problemas[i].turnos
        }
    }
    println(problemasResueltos)
    println("Has resuelto $resueltos de 5 problemas con $turnosTotales intento/s totales")
    for (i in problemasResueltos){
        when(i){
            '0'-> print("\n" + "En el primer problema has necesitado ${turnos[0]} turno/s para resolverlo")
            '1'-> print("\n" + "En el segundo problema has necesitado ${turnos[1]} turno/s para resolverlo")
            '2'-> print("\n" + "En el tercer problema has necesitado ${turnos[2]} turno/s para resolverlo")
            '3'-> print("\n" + "En el primer problema has necesitado ${turnos[3]} turno/s para resolverlo")
            '4'-> print("\n" + "En el primer problema has necesitado ${turnos[4]} turno/s para resolverlo")
        }
    }
}

/**
 * Función que modifica la lista turnos con los turnos actuales de cada problema en específico
 * @author Fabio Cebral
 * @param turnos Lista en la que cada posicion es el turno del problema en esa posición
 * @param problemas Lista con todos los problemas
 * @return devuelve la lista de turnos modificadas
 */
fun getTurns(turnos:MutableList<Int>,problemas:MutableList<Problema>): MutableList<Int> {
    for (i in problemas.indices){
        turnos[i]=problemas[i].turnos
    }
    return turnos
}