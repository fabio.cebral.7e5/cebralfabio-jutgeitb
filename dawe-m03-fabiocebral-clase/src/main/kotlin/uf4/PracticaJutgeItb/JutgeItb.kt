package uf4.PracticaJutgeItb

import uf2.examen.ANSI_RESET
import wordleConFicheros.ANSI_BLUE
import java.lang.ref.SoftReference

class Problema(_enunciado:String,_pregunta:String,_respuesta:String,_turnos:Int,_resolt:Boolean,_respuestasIntroducidas:MutableList<String>){
    var enunciado=_enunciado
    var pregunta=_pregunta
    var respuesta=_respuesta
    var turnos=_turnos
    var resolt=_resolt
    val respuestasIntroducidas=_respuestasIntroducidas
    override fun toString(): String {
        println(ANSI_BLUE +enunciado + ANSI_RESET + "¿Lo quieres solucionar?")
        return enunciado
    }
    fun showProblem(){
        println(ANSI_RESET + pregunta)
    }

}