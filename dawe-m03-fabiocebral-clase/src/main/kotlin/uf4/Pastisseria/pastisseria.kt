package uf4.Pastisseria

class Producte (_nom:String, _preu:Double){
    var nom:String=_nom
    var preu:Double = _preu
}
class Pastis(_producte: Producte, _pes:Int, _calorias:Int){
    var producte: Producte =_producte
    var pes:Int=_pes
    var calorias:Int=_calorias
    override fun toString(): String {
       return "${producte.nom} - ${producte.preu}€ - ${pes} gramos - ${calorias} calorias"
    }
}
class Beguda(_producte: Producte, _increment:Boolean){
    var producte: Producte =_producte
    var increment:Boolean=_increment
    override fun toString(): String {
        return "${producte.nom} - ${producte.preu}€ - ${increment}"
    }
}
