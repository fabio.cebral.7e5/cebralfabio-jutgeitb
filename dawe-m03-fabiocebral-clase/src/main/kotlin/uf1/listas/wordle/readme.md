# WORDLE (FABIO CEBRAL)
## Explicación del juego
### Se trata del clásico juego Wordle en el cual el jugador introducirá hasta 6 palabras hasta acertar la palabra secreta. Para que sea posible para el usuario, el juego le ofrecerá una serie de pistas. Por ejemplo, si la letra pertenece a la palabra y esta en la posición correcta mostrará la letra en verde, si la letra esta en la palabra pero no es la posición correcta se mostrara la letra en amarillo y si la letra  no pertenece a la palabra se le mostrará en gris. Si  en 6 turnos el usuario no es capaz de acertar la palabra perderá. Al perder se le dará la opción de volver a jugar o retirarse, si decide dejar de jugar se le mostraran sus estadísticas

## ESTADÍSTICAS
### Al abandonar el juego se le mostrará al usuario las siguientes estadísticas: 
1. PARTIDAS JUGADAS
2. PALABRAS ACERTADAS
3. PALABRAS FALLADAS
4. PORCENTAJE DE ACIERTO
5. RACHA MAS ALTA
6. PORCENTAJE DE ACIERTO POR CADA TURNO
## VARIABLES
1. input: La palabra introducida por el usuario.
1. secretWord: La palabra que debe acertar que esta extraida de la lista creada en la parte superior.
1. wordList: Es una lista dentro de otra lista que crea el tablero con las palabras introducidas
1. playerTurns: Controla los turnos del usuario.
2. replica: Replica de la palabra secreta utilizada para controlar las repeticiones.
3. paraulesResoltes: Contador para las palabras acertadas.
4. paraulesNoResoltes: Contador para las palabras no resueltas.
5. partidasJugadas: Contador para las partidas jugadas.
6. rachaActual: Contador de la racha actual para ver si supera la racha mas alta
7. rachaMasAlta: Variable que guarda la racha mas alta
8. itsTheTurnOfGuess: Lista de 6 posiciones llena de zeros y cada posición guarda un turno y se le sumara un 1 dependiendo el turno en que se acierte
## Errores detectados

## Opinión Práctica
### Aunque se que esto no es necesario me gustaría ofrecerte mi feedback acerca de la actividad, en mi experiencia personal ha sido una actividad muy divertida, aunque he visto a muchos de mis compañeros muy extresados con la práctica creo que si te esfuerzas la consigues y ademas siempre resuelves todas las dudas personales que puedes. Espero que las actividades mantengan esta línea.